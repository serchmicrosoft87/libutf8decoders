// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#ifndef LIBUTF8DECODERS_TESTS_UTILITIES_HPP
#define LIBUTF8DECODERS_TESTS_UTILITIES_HPP

#include "libutf8decoders/config.hpp"
#include "libutf8decoders/features.hpp"

#include <string>

#include "libutf8decoders/types.hpp"

namespace utf8decoders {
namespace tests {

using ::std::string;

using ::utf8decoders::u8_char_t;
using ::utf8decoders::size_t;

static U8D_CONSTEXPR_DECL const char hex_map[] = {'0', '1', '2', '3', '4', '5', '6', '7',
                                                  '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

string
to_hex(const u8_char_t* bytes, size_t count);

} // namespace tests
} // namespace utf8decoders

#endif /* LIBUTF8DECODERS_TESTS_UTILITIES_HPP */

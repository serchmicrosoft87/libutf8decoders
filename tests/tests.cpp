// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#include <cstdint>
#include <string>

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include "libutf8decoders/utf8decoders.hpp"

#include "tests/bad_u8.hpp"
#include "tests/types.hpp"
#include "tests/utilities.hpp"

namespace utf8decoders {
namespace tests {

static const iconv ic("UTF-32LE", "UTF-8");

using ::std::string;

static U8D_CONSTEXPR_DECL const size_t num_decode_next_impls =
    sizeof(decode_next_impls) / sizeof(decode_next_impl);
static U8D_CONSTEXPR_DECL const size_t num_decode_impls =
    sizeof(decode_impls) / sizeof(decode_impl);

TEST_CASE("decode_next") {
    for (size_t i = 0; i < num_decode_next_impls; ++i) {
        string name = decode_next_impls[i].name;
        decode_next_f dn = decode_next_impls[i].f;
        u32_char_t cp{};
#ifdef U8D_HAVE_BOOST_TEXT
        bool is_boost_text =
            name.find("boost_text::decode_next", 0, 23) != string::npos;
#endif
#ifdef U8D_HAVE_ICU
        bool is_icu_u8_next_unsafe =
            name.find("icu::u8_next_unsafe", 0, 19) != string::npos;
#endif
        SECTION(string("impl ") + name) {
            SECTION("empty input") {
                u8_s_t s;
                REQUIRE(
#ifdef U8D_USE_STD_STRING_VIEW
                    dn(s, cp) ==
#else
                    dn(&*s.cbegin(), s.size(), cp) ==
#endif
                    0 );
            }
            SECTION("bad sequences") {
#ifdef U8D_HAVE_ICU
                // ICU u8_next_unsafe assumes valid UTF-8
                if (is_icu_u8_next_unsafe)
                    continue;
#endif
                for (auto& s : bad_u8_sequences) {
                    SECTION(to_hex(&*s.cbegin(), s.size())) {
                        REQUIRE(
#ifdef U8D_USE_STD_STRING_VIEW
                            dn(s, cp) ==
#else
                            dn(&*s.cbegin(), s.size(), cp) ==
#endif
                            static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::ERROR) );
                    }
                }
            }
            SECTION("round-trip") {
                using InputIt = iconv::InputIt;
                using OutputIt = iconv::OutputIt;
                using size_t = iconv::size_t;

                static U8D_CONSTEXPR_DECL const ::std::uint32_t n_src_units = 1;
                static U8D_CONSTEXPR_DECL const ::std::uint32_t n_dst_units = 4;
                u32_s_t src;
                src.resize(n_src_units);
                u8_s_t dst;
                dst.resize(n_dst_units);

                for (u32_char_t ecp = 0; ecp < SPUA_B_END; ++ecp) {
#ifdef U8D_HAVE_BOOST_TEXT
                    bool is_reserved_noncharacter =
                        ::boost::text::utf8::reserved_noncharacter(ecp);
                    if ((ecp >= SURROGATE_BASE && ecp < BMP_PRIVATE_BASE) ||
                        (is_boost_text &&
                         ecp >= BMP_RESERVED_NONCHARACTER_BASE &&
                         ecp < BMP_RESERVED_NONCHARACTER_END))
                        continue;
#else
                    if (ecp >= SURROGATE_BASE && ecp < BMP_PRIVATE_BASE)
                        continue;
#endif

                    src[0] = ecp;

                    InputIt src_begin = reinterpret_cast<InputIt>(&*src.begin());
                    size_t src_bytes = n_src_units * sizeof(u32_s_t::value_type);
                    u8_s_t::const_pointer dst_begin = &*dst.cbegin();
                    OutputIt dst_it = reinterpret_cast<OutputIt>
                        (const_cast<u8_s_t::pointer>(dst_begin));
                    size_t dst_bytes = n_dst_units * sizeof(u8_s_t::value_type);
                    size_t dst_size = dst_bytes;

                    ic.convert_with_check(src_begin, src_bytes, dst_it, dst_bytes);
                    dst_size -= dst_bytes / sizeof(u8_s_t::value_type);

                    ptrdiff_t r =
#ifdef U8D_USE_STD_STRING_VIEW
                        dn({dst_begin, dst_size}, cp);
#else
                        dn(dst_begin, dst_size, cp);
#endif
#ifdef U8D_HAVE_BOOST_TEXT
                    if (!is_boost_text) {
                        REQUIRE(r == dst_size);
                        REQUIRE(ecp == cp);
                    } else {
                        if (ecp != REPLACEMENT_CHARACTER && !is_reserved_noncharacter) {
                            REQUIRE(r != static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::ERROR));
                            REQUIRE(ecp == cp);
                        } else {
                            // there's no way to distinguish the result from an
                            // error when the input is the replacement character
                            REQUIRE(r == static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::ERROR));
                            REQUIRE(REPLACEMENT_CHARACTER == cp);
                        }
                    }
#else
                    REQUIRE(r == dst_size);
                    REQUIRE(ecp == cp);
#endif
                }
            }
        }
    }
}

TEST_CASE("decode") {
    for (size_t i = 0; i < num_decode_impls; ++i) {
        string name = decode_impls[i].name;
        decode_f d = decode_impls[i].f;
        u32_s_t dst;
        dst.resize(1);
#ifdef U8D_HAVE_AV
        bool is_av =
            name.find("av::decode", 0, 10) != string::npos;
#endif
#ifdef U8D_HAVE_BOOST_TEXT
        bool is_boost_text =
            name.find("boost_text::decode", 0, 18) != string::npos;
#endif
#ifdef U8D_HAVE_ICU
        bool is_icu_u8_next_unsafe =
            name.find("icu::u8_next_unsafe", 0, 19) != string::npos;
        bool is_icu_str_from_utf8_lenient =
            name.find("icu::decode<::u_strFromUTF8Lenient>", 0, 35) != string::npos;
#endif
#ifdef U8D_HAVE_PYTHON
        bool is_python =
            name.find("python::decode", 0, 14) != string::npos;
#endif
        SECTION(string("impl ") + name) {
            SECTION("empty input") {
                u8_s_t s;
                REQUIRE(
#ifdef U8D_USE_STD_STRING_VIEW
                    d(s, dst, 0) ==
#else
                    d(&*s.cbegin(), s.size(), dst, 0) ==
#endif
                    0 );
            }
            SECTION("bad sequences") {
#ifdef U8D_HAVE_AV
                // av does not properly validate
                if (is_av)
                    continue;
#endif
#ifdef U8D_HAVE_ICU
                // ICU u8_next_unsafe and u_strFromUTF8Lenient assume valid UTF-8
                if (is_icu_u8_next_unsafe || is_icu_str_from_utf8_lenient)
                    continue;
#endif
                bool outputs_replacement = false;
#ifdef U8D_HAVE_BOOST_TEXT
                if (is_boost_text)
                    outputs_replacement = true;
#endif
#ifdef U8D_HAVE_PYTHON
                if (is_python)
                    outputs_replacement = true;
#endif
                for (auto& s : bad_u8_sequences) {
                    SECTION(to_hex(&*s.cbegin(), s.size())) {
                        ptrdiff_t r =
#ifdef U8D_USE_STD_STRING_VIEW
                            d(s, dst, 0);
#else
                            d(&*s.cbegin(), s.size(), dst, 0);
#endif
                        if (!outputs_replacement)
                            REQUIRE(r == static_cast<ptrdiff_t>(DECODE_STATUS::ERROR));
                        else
                            REQUIRE(r > static_cast<ptrdiff_t>(DECODE_STATUS::ERROR));
                    }
                }
            }
            SECTION("round-trip") {
                using InputIt = iconv::InputIt;
                using OutputIt = iconv::OutputIt;
                using size_t = iconv::size_t;

                static U8D_CONSTEXPR_DECL const ::std::uint32_t n_src_units = 1;
                static U8D_CONSTEXPR_DECL const ::std::uint32_t n_u8_dst_units = 4;
                u32_s_t src;
                src.resize(n_src_units);
                dst.resize(n_src_units);
#ifdef U8D_HAVE_ICU
                if (is_icu_str_from_utf8_lenient)
                    // requires dst.size() * 2 [UChar] >= src.size()
                    dst.resize(2);
#endif
                u8_s_t u8_dst;
                u8_dst.resize(n_u8_dst_units);
                u32_char_t cp{};

                for (u32_char_t ecp = 0; ecp < SPUA_B_END; ++ecp) {
#ifdef U8D_HAVE_BOOST_TEXT
                    bool is_reserved_noncharacter =
                        ::boost::text::utf8::reserved_noncharacter(ecp);
                    if ((ecp >= SURROGATE_BASE && ecp < BMP_PRIVATE_BASE) ||
                        (is_boost_text &&
                         ecp >= BMP_RESERVED_NONCHARACTER_BASE &&
                         ecp < BMP_RESERVED_NONCHARACTER_END))
                        continue;
#else
                    if (ecp >= SURROGATE_BASE && ecp < BMP_PRIVATE_BASE)
                        continue;
#endif

                    src[0] = ecp;

                    InputIt src_begin = reinterpret_cast<InputIt>(&*src.begin());
                    size_t src_bytes = n_src_units * sizeof(u32_s_t::value_type);
                    u8_s_t::const_pointer u8_dst_begin = &*u8_dst.cbegin();
                    OutputIt u8_dst_it = reinterpret_cast<OutputIt>
                        (const_cast<u8_s_t::pointer>(u8_dst_begin));
                    size_t u8_dst_bytes = n_u8_dst_units * sizeof(u8_s_t::value_type);
                    size_t u8_dst_size = u8_dst_bytes;

                    ic.convert_with_check(src_begin, src_bytes, u8_dst_it, u8_dst_bytes);
                    u8_dst_size -= u8_dst_bytes / sizeof(u8_s_t::value_type);

                    ptrdiff_t r =
#ifdef U8D_USE_STD_STRING_VIEW
                        d({u8_dst_begin, u8_dst_size}, dst, 0);
#else
                        d(u8_dst_begin, u8_dst_size, dst, 0);
#endif
                    cp = dst[0];
#ifdef U8D_HAVE_BOOST_TEXT
                    REQUIRE(r == 1);
                    if (!is_boost_text) {
                        REQUIRE(ecp == cp);
                    } else {
                        if (ecp != REPLACEMENT_CHARACTER && !is_reserved_noncharacter) {
                            REQUIRE(ecp == cp);
                        } else {
                            // there's no way to distinguish the result from an
                            // error when the input is the replacement character
                            REQUIRE(REPLACEMENT_CHARACTER == cp);
                        }
                    }
#else
                    REQUIRE(r == 1);
                    REQUIRE(ecp == cp);
#endif
                }
            }
        }
    }
}

} // namespace tests
} // namespace utf8decoders

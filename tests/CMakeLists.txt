find_package(Catch2 CONFIG REQUIRED)

include(CTest)
include(Catch)

add_executable(tests
  tests.cpp
  utilities.cpp
  utilities.hpp
  bad_u8.hpp
  types.hpp
)

target_link_libraries(tests PRIVATE Catch2::Catch2 utf8decoders::utf8decoders)

set(U8D_TESTS_COMPILER_WARNINGS ${U8D_COMPILER_WARNINGS})
# embedding a directive within macro arguments
list(REMOVE_ITEM U8D_TESTS_COMPILER_WARNINGS
  ${U8D_COMPILER_OPTION_PEDANTIC_ERRORS}
)
if (U8D_WITH_BOOST_TEXT)
  list(REMOVE_ITEM U8D_TESTS_COMPILER_WARNINGS
    ${U8D_COMPILER_OPTION_WSIGN_CONVERSION}
  )
  if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
    list(REMOVE_ITEM U8D_TESTS_COMPILER_WARNINGS
      ${U8D_COMPILER_OPTION_WEXTRA}
    )
    list(REMOVE_ITEM U8D_TESTS_COMPILER_WARNINGS
      ${U8D_COMPILER_OPTION_WCONVERSION}
    )
  endif ()
endif ()
if (U8D_WITH_ICU)
  # ICU U8_NEXT conversion from int to uint8_t
  list(REMOVE_ITEM U8D_TESTS_COMPILER_WARNINGS
    ${U8D_COMPILER_OPTION_WCONVERSION}
  )
endif ()

target_compile_options(tests PRIVATE
  ${U8D_TESTS_COMPILER_WARNINGS}
)

target_include_directories(tests PUBLIC
  ${CMAKE_CURRENT_SOURCE_DIR}/..
)

catch_discover_tests(tests)

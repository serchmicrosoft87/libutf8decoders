// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#ifndef LIBUTF8DECODERS_BENCHMARKS_DATA_HPP
#define LIBUTF8DECODERS_BENCHMARKS_DATA_HPP

#include "libutf8decoders/features.hpp"

namespace utf8decoders {
namespace benchmarks {

#ifdef U8D_BENCHMARKS_HAVE_STRESS_TESTS
static U8D_CONSTEXPR_DECL const char* STRESS_DATA[] = {
     "stress/0",
     "stress/1",
     "stress/2"
};

static U8D_CONSTEXPR_DECL const size_t num_stress_tests =
    sizeof(STRESS_DATA) / sizeof(STRESS_DATA[0]);
#endif

#ifdef U8D_BENCHMARKS_HAVE_WIKI
static U8D_CONSTEXPR_DECL const char* WIKI_DATA[] = {
     "wiki/English_language-en",
     "wiki/Hindi_language-hi",
     "wiki/Japanese_language-ja",
     "wiki/Korean_language-ko",
     "wiki/Portuguese_language-pt",
     "wiki/Russian_language-ru",
     "wiki/Swedish_language-sv",
     "wiki/Chinese_language-zh",
};

static U8D_CONSTEXPR_DECL const size_t num_wiki =
    sizeof(WIKI_DATA) / sizeof(WIKI_DATA[0]);
#endif

} // namespace benchmarks
} // namespace utf8decoders

#endif /* LIBUTF8DECODERS_BENCHMARKS_DATA_HPP */

// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#include "libutf8decoders/config.hpp"
#include "benchmarks/config.hpp"

#include <string>

#include <benchmark/benchmark.h>

#include "libutf8decoders/utf8decoders.hpp"

#include "benchmarks/data.hpp"
#include "benchmarks/types.hpp"
#include "benchmarks/utilities.hpp"

using ::std::string;

namespace utf8decoders {
namespace benchmarks {

template<decode_f d>
static void
bench_decode(::benchmark::State& st, const string& filename)
    noexcept {
    u8_s_t src = read_file(filename.c_str());
    size_t src_size = src.size();
    u32_s_t dst;
    dst.resize(src_size);
    ptrdiff_t cp = 0;
    for (auto _ : st) {
#ifdef U8D_USE_STD_STRING_VIEW
        ::benchmark::DoNotOptimize(cp = d(src, dst, 0));
#else
        ::benchmark::DoNotOptimize(cp = d(&*src.cbegin(), src_size, dst, 0));
#endif
#ifdef U8D_BENCHMARKS_HAVE_DECODE_ERROR_CHECK
        if (cp == static_cast<ptrdiff_t>(DECODE_STATUS::ERROR))
            st.SkipWithError("decode error");
#endif
    }
}

#define REGISTER_DECODE_BENCHMARK(f)                                    \
    name = string(#f) + " " + filename;                                 \
    ::benchmark::RegisterBenchmark(name.c_str(),                        \
                                   &bench_decode<f>,                    \
                                   filename);

void
register_decode_benchmarks(const string& filename) noexcept {
    string name;
#ifdef U8D_HAVE_AV
    REGISTER_DECODE_BENCHMARK(av::decode)
#endif
#ifdef U8D_HAVE_BH
    REGISTER_DECODE_BENCHMARK(bh::decode<bh::decode_next_stateful_branchless_0>)
    REGISTER_DECODE_BENCHMARK(bh::decode<bh::decode_next_stateful_branchless_1>)
    REGISTER_DECODE_BENCHMARK(bh::decode<bh::decode_next_stateful_branching_0>)
#endif
#ifdef U8D_HAVE_BOOST_TEXT
    REGISTER_DECODE_BENCHMARK(boost_text::decode)
#endif
#ifdef U8D_HAVE_BRANCHLESS
    REGISTER_DECODE_BENCHMARK(branchless::decode)
#endif
#ifdef U8D_HAVE_CODECVT
    REGISTER_DECODE_BENCHMARK(codecvt::decode)
#endif
#ifdef U8D_HAVE_ICONV
    REGISTER_DECODE_BENCHMARK(iconv::decode)
#endif
#ifdef U8D_HAVE_ICU
    REGISTER_DECODE_BENCHMARK(decode<icu::u8_next>)
    REGISTER_DECODE_BENCHMARK(decode<icu::u8_next_unsafe>)
    REGISTER_DECODE_BENCHMARK(icu::decode<::u_strFromUTF8>)
    REGISTER_DECODE_BENCHMARK(icu::decode<::u_strFromUTF8Lenient>)
#endif
#ifdef U8D_HAVE_KEWB
    REGISTER_DECODE_BENCHMARK(kewb::decode<::uu::UtfUtils::BasicSmallTableConvert>)
    REGISTER_DECODE_BENCHMARK(kewb::decode<::uu::UtfUtils::FastSmallTableConvert>)
    REGISTER_DECODE_BENCHMARK(kewb::decode<::uu::UtfUtils::SseSmallTableConvert>)
    REGISTER_DECODE_BENCHMARK(kewb::decode<::uu::UtfUtils::BasicBigTableConvert>)
    REGISTER_DECODE_BENCHMARK(kewb::decode<::uu::UtfUtils::FastBigTableConvert>)
    REGISTER_DECODE_BENCHMARK(kewb::decode<::uu::UtfUtils::SseBigTableConvert>)
#endif
#ifdef U8D_HAVE_LLVM
    REGISTER_DECODE_BENCHMARK(llvm::decode)
#endif
#ifdef U8D_HAVE_MK
    REGISTER_DECODE_BENCHMARK(decode<mk::decode_next<mk::icu::decode_next>>)
    REGISTER_DECODE_BENCHMARK(decode<mk::decode_next<mk::kewb::decode_next>>)
#endif
#ifdef U8D_HAVE_PYTHON
    REGISTER_DECODE_BENCHMARK(python::decode)
#endif
#ifdef U8D_HAVE_UTF8PROC
    REGISTER_DECODE_BENCHMARK(decode<utf8proc::decode_next>)
#endif
}

#undef REGISTER_DECODE_BENCHMARK

} // namespace benchmarks
} // namespace utf8decoders

namespace u8d_bench = ::utf8decoders::benchmarks;

void
register_decode_benchmarks() {
    string data_dir = u8d_bench::DATA_DIR;
    data_dir += "/";
#ifdef U8D_BENCHMARKS_HAVE_STRESS_TESTS
    for (size_t i = 0; i < u8d_bench::num_stress_tests; ++i) {
        string filename = data_dir + u8d_bench::STRESS_DATA[i];
        u8d_bench::register_decode_benchmarks(filename);
    }
#endif
#ifdef U8D_BENCHMARKS_HAVE_WIKI
    for (size_t i = 0; i < u8d_bench::num_wiki; ++i) {
        string filename = data_dir + u8d_bench::WIKI_DATA[i];
        u8d_bench::register_decode_benchmarks(filename);
    }
#endif
}

int
main(int argc, char* argv[]) {
    register_decode_benchmarks();
    ::benchmark::Initialize(&argc, argv);
    ::benchmark::RunSpecifiedBenchmarks();
}

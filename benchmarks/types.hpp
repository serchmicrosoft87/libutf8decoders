// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#ifndef LIBUTF8DECODERS_BENCHMARKS_TYPES_HPP
#define LIBUTF8DECODERS_BENCHMARKS_TYPES_HPP

#include "libutf8decoders/config.hpp"
#include "libutf8decoders/features.hpp"

#include "libutf8decoders/api.hpp"
#include "libutf8decoders/types.hpp"

namespace utf8decoders {
namespace benchmarks {

using ::utf8decoders::size_t;
using ::utf8decoders::ptrdiff_t;
using ::utf8decoders::u8_char_t;
using ::utf8decoders::u32_char_t;
using ::utf8decoders::u8_s_t;
using ::utf8decoders::u32_s_t;
#ifdef U8D_USE_STD_STRING_VIEW
using ::utf8decoders::u8_sv_t;
#else
using InputIt = const u8_char_t*;
#endif

using ::utf8decoders::DECODE_STATUS;

using ::utf8decoders::decode_f;

} // namespace benchmarks
} // namespace utf8decoders

#endif /* LIBUTF8DECODERS_BENCHMARKS_TYPES_HPP */

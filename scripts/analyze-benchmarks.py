#!/usr/bin/python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: MIT
# Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

import fnmatch
import glob
import json
import os

from argparse import ArgumentParser
from collections import defaultdict

import matplotlib as mpl
import numpy as np

GOOGLE_BENCHMARK_AGGREGATE_NAMES = ['mean', 'median', 'stddev', 'BigO', 'RMS']
SI_RATIOS = {'': 1e0, 'm': 1e-3, 'u': 1e-6, 'n': 1e-9}


def _create_parser():
    parser = ArgumentParser(description="""Analyzes and plots benchmark results""")
    parser.add_argument('-f', '--filter',
                        dest='f',
                        help="Filter pattern to apply to filenames",
                        default='*')
    parser.add_argument('-r', '--recursive',
                        dest='r',
                        action="store_true",
                        help="Whether or not to search directories recursively",
                        default=False)
    parser.add_argument('-p', '--plot',
                        dest='p',
                        action="store_true",
                        help="Whether or not to plot figures",
                        default=False)
    parser.add_argument('-fo', '--figure-output',
                        dest='fo',
                        action="store_true",
                        help="Whether or not to output figures",
                        default=False)
    parser.add_argument('-fd', '--figure-output-dir',
                        dest='fd',
                        help="Destination directory for output figures",
                        default=os.getcwd())
    parser.add_argument('-ff', '--figure-output-format',
                        dest='ff',
                        help="Output format for figures",
                        default="eps")
    parser.add_argument('-fbe', '--figure-backend',
                        dest='fbe',
                        help="matplotlib backend")
    parser.add_argument('-nc', '--no-clobber',
                        dest='nc',
                        action="store_true",
                        help="Prevents overwrite of existing figures",
                        default=False)
    parser.add_argument('-fw', '--figure-width',
                        dest='fw',
                        type=float,
                        help="Figure width in inches",
                        default=8.)
    parser.add_argument('-fh', '--figure-height',
                        dest='fh',
                        type=float,
                        help="Figure height in inches",
                        default=6.)
    parser.add_argument('-ptitle', '--plot-title',
                        dest='ptitle',
                        help="Plot title prefix")
    parser.add_argument('-psort', '--plot-sort',
                        dest='psort',
                        action='store_true',
                        default=False,
                        help="Sort plot by time")
    parser.add_argument('-ptime', '--plot-time',
                        dest='ptime',
                        choices=['real', 'cpu'],
                        help="Which time to plot",
                        default='cpu')
    parser.add_argument('-si', '--si-prefix',
                        dest='si',
                        choices=list(SI_RATIOS.keys()),
                        help="SI prefix for time analysis and plots",
                        default='m')
    parser.add_argument('-V', '--verbose',
                        action='store_true',
                        default=False,
                        help="Enable verbose output")
    parser.add_argument('dir',
                        nargs='*',
                        help="Directories to search for benchmarks",
                        default=os.getcwd())
    return parser

def save_figures(figs=None, output_dir=".", overwrite=False, file_format="eps",
                 prefix="", suffix="", fig_size=(8., 6.), **kwargs):
    """
    Saves the figures given in fig_numbers to output_directory

    Parameters

    fig_nums : list, optional
       List of integers for the figure numbers and/or
       matplotlib.figure.Figure to save
    output_dir : string, optional
        Output directory for the figures. Defaults to "."
    overwrite : bool, optional
        Whether to overwrite existing files
    file_format : string, optional
        Format in which to save figures
    prefix : string, optional
        Prefix prepended to the beginning of each file name
    suffix : string, optional
        Suffix appended to the end of each file name, before the file extension
    fig_size : tuple, optional
        Figure size in inches, (w,h)
    kwargs : dict
        Keyword args passed onto savefig
    """
    import matplotlib.pyplot as plt
    fig_nums = plt.get_fignums()
    if figs is None:
        figs = [plt.figure(fig_num) for fig_num in fig_nums]
    fig_str_format = "%0" + str(len(str(max(fig_nums)))) + "d"
    for fig in figs:
        if type(fig) is int:
            fig = plt.figure(fig)
        pathname = output_dir + "/"
        if prefix:
            pathname += prefix + "_"
        pathname += "f" + fig_str_format % fig.number
        if suffix:
            pathname += "_" + suffix
        pathname += "." + file_format
        if not overwrite:
            if os.path.isfile(pathname):
                print("File '%s' exists. Skipping" % pathname)
                continue
        fig.set_size_inches(fig_size,forward=True)
        fig.savefig(pathname, **kwargs)

def get_files(dirs, pattern='*', recursive=False):
    """
    Searches the directories `dirs' to find files.

    Parameters:

    dirs : list of strings
        Directories to search for files
    pattern: string
        Pattern to filter file names
    recursive: boolean
        Whether to recursively search the directories

    Returns:

    files : list of strings
        Files matching pattern in the directories specified
    """
    files = []
    for d in dirs:
        if os.path.isdir(d):
            pathnames = []
            if not recursive:
                pathnames = [pathname for pathname in glob.glob(d + "/" + pattern)
                             if os.path.isfile(pathname)]
            else:
                for dirpath, dirnames, filenames in os.walk(d):
                    for filename in fnmatch.filter(filenames, pattern):
                        pathnames.append(os.path.join(dirpath, filename))
            for pathname in pathnames:
                files.append(pathname)
    return files

def parse_args():
    return _create_parser().parse_args()

def _process_args(args):
    if args.fbe:
        mpl.use(args.fbe)

def _cpu_string(context):
    return "{:d} CPUs @ {:d} MHz".format(context['num_cpus'], context['mhz_per_cpu'])

def _extract_benchmark_context(bench_json):
    context = bench_json['context']
    # remove un-needed metadata
    context.pop('date')
    cpu_str = _cpu_string(context)
    return json.dumps(context), cpu_str

def _is_aggregate_result(name):
    for aggregate_name in GOOGLE_BENCHMARK_AGGREGATE_NAMES:
        if name.endswith("_%s" % aggregate_name):
            return True
    return False

def _extract_benchmark_results(bench_json):
    benchmarks = bench_json['benchmarks']
    bench_results = defaultdict(lambda: defaultdict(list))
    for benchmark in benchmarks:
        if _is_aggregate_result(benchmark['name']):
            continue
        name = benchmark.pop('name').split(' ')
        decoder = name[0]
        input_dataset = ' '.join(name[1:])
        input_dataset = "{:s}/{:s}".format(os.path.basename(os.path.dirname(input_dataset)),
                                           os.path.basename(input_dataset))
        bench_results[input_dataset][decoder].append(benchmark)
    return bench_results

def _update_benchmark_results(r0, r1):
    r0_keys = set()
    for input_dataset in r0:
        for decoder in r0[input_dataset]:
            r0[input_dataset][decoder].append(r1[input_dataset][decoder])
            r0_keys.add((input_dataset, decoder))
    r1_keys = set()
    for input_dataset in r1:
        for decoder in r1:
            r1_keys.add((input_dataset, decoder))
    new_keys = r1_keys - r0_keys
    if new_keys:
        for input_dataset, decoder in new_keys:
            r0[input_dataset][decoder].append(r1[input_dataset][decoder])

def _read_json(f):
    bench_json = json.load(f)
    bench_context, cpu_str = _extract_benchmark_context(bench_json)
    bench_results = _extract_benchmark_results(bench_json)
    return bench_context, cpu_str, bench_results

def read_files(files, verbose=False):
    bench_data = dict()
    for fname in files:
        if verbose:
            print("Processing file '%s'" % fname)
        with open(fname, 'rb') as f:
            bench_context, cpu_str, bench_results = _read_json(f)
            if bench_context in bench_data:
                if verbose:
                    print("Updating existing benchmark context with cpu_str='%s'" % cpu_str)
                _update_benchmark_results(bench_data[bench_context]['results'], bench_results)
            else:
                if verbose:
                    print("Found new benchmark context with cpu_str='%s'" % cpu_str)
                bench_data[bench_context] = {'cpu_str': cpu_str,
                                             'results': bench_results}
    return bench_data

def _console_size():
    return os.popen('stty size', 'r').read().split()

def _list_of_dict_to_dict_of_ndarray(lod, si_prefix=''):
    dn = {k: np.array([d[k] for d in lod])
          for k in lod[0] if k != 'time_unit'}
    for i, d in enumerate(lod):
        u = d['time_unit']
        time_scale = SI_RATIOS[u[0]] / SI_RATIOS[si_prefix]
        dn['real_time'][i] *= time_scale
        dn['cpu_time'][i] *= time_scale
    return dn

def process_bench_data(bench_data, si_prefix='', verbose=False):
    if verbose:
        _, console_width = _console_size()
        decoder_col_width = int(console_width) * 3 // 5
        data_col_width = (int(console_width) - decoder_col_width) // 3
        float_precision = min(5, data_col_width - 7)
        data_table_header_format = "{:<" + str(decoder_col_width) + "}"
        data_table_header_format += 3 * ("{:>" + str(data_col_width) + "}")
        data_table_data_format = "{:<" + str(decoder_col_width) + "}"
        data_table_data_format += 2 * ("{:>" + str(data_col_width) + "." + str(float_precision) + "e}")
        data_table_data_format += "{:>" + str(data_col_width) + "d}"
        units = si_prefix + 's'
    for context in bench_data:
        if verbose:
            print(("{:=^" + console_width + "}").format(''))
            print("Context: %s" % bench_data[context]['cpu_str'])
            print(("{:=^" + console_width + "}").format(''))
        bench_results = bench_data[context]['results']
        for input_dataset in bench_results:
            if verbose:
                print(("{:-^" + console_width + "}").format(''))
                print("dataset: %s" % input_dataset)
                print(("{:-^" + console_width + "}").format(''))
                print(data_table_header_format.format('decoder',
                                                      'real ' + units,
                                                      'CPU ' + units,
                                                      'iterations'))
                print(("{:-^" + console_width + "}").format(''))
            benchmark = bench_results[input_dataset]
            for decoder in benchmark:
                benchmark[decoder] = _list_of_dict_to_dict_of_ndarray(benchmark[decoder],
                                                                      si_prefix)
                num_trials = np.size(benchmark[decoder]['iterations'])
                total_iterations = np.sum(benchmark[decoder]['iterations'])
                trial_weights = benchmark[decoder]['iterations'] / total_iterations
                mean_real_time = np.average(benchmark[decoder]['real_time'],
                                            weights=trial_weights)
                mean_cpu_time = np.average(benchmark[decoder]['cpu_time'],
                                            weights=trial_weights)
                benchmark[decoder]['num_trials'] = num_trials
                benchmark[decoder]['total_iterations'] = total_iterations
                benchmark[decoder]['mean_real_time'] = mean_real_time
                benchmark[decoder]['mean_cpu_time'] = mean_cpu_time
                if num_trials > 1:
                    mean_real_time_error = np.std(benchmark[decoder]['real_time'] *
                                                  trial_weights,
                                                  ddof=1) / np.sqrt(num_trials)
                    mean_cpu_time_error = np.std(benchmark[decoder]['cpu_time'] *
                                                  trial_weights,
                                                  ddof=1) / np.sqrt(num_trials)
                    benchmark[decoder]['mean_real_time_error'] = mean_real_time_error
                    benchmark[decoder]['mean_cpu_time_error'] = mean_cpu_time_error
                if verbose:
                    print(data_table_data_format.format(decoder,
                                                        mean_real_time,
                                                        mean_cpu_time,
                                                        total_iterations))

def plot_bench_data(bench_data, title_prefix=None, sort=False, time_type='cpu', si_prefix='m',
                    fw=8, fh=6):
    import matplotlib.pyplot as plt
    xlabel_suffix = "time (" + si_prefix + "s)"
    for context in bench_data:
        bench_results = bench_data[context]['results']
        for input_dataset in bench_results:
            if title_prefix:
                ptitle = "%s" % title_prefix
            else:
                ptitle = ""
            ptitle += "%s: %s" % (bench_data[context]['cpu_str'],
                                  input_dataset)
            benchmark = bench_results[input_dataset]
            plt.figure(figsize=(fw, fh))
            plt.xlabel("%s %s" % (time_type, xlabel_suffix))
            plt.ylabel("decoder")
            plt.suptitle(ptitle)
            num_decoders = len(benchmark)
            decoders = np.empty(num_decoders, dtype=object)
            mean_times = np.empty(num_decoders)
            mean_times_error = np.empty(num_decoders)
            for i, decoder in enumerate(benchmark):
                decoders[i] = decoder
                mean_times[i] = benchmark[decoder]['mean_'+time_type+'_time']
                if benchmark[decoder]['num_trials'] > 1:
                    mean_times_error[i] = benchmark[decoder]['mean_'+time_type+'_time_error']
                else:
                    mean_times_error[i] = 0
            y = np.arange(0, num_decoders)
            if sort:
                s_idx = np.argsort(mean_times)
                x = mean_times[s_idx]
                xerr = mean_times_error[s_idx]
                tick_label = decoders[s_idx]
            else:
                x = mean_times
                xerr = mean_times_error
                tick_label = decoders
            plt.barh(y, x, xerr=xerr, tick_label=tick_label)
            plt.tight_layout(rect=[0, 0, 1, 0.95])

def save_bench_data(bench_data, args):
    import matplotlib.pyplot as plt
    fi = 1
    for context in bench_data:
        bench_results = bench_data[context]['results']
        for input_dataset in bench_results:
            if args.ptitle:
                prefix = "%s" % args.ptitle.strip().replace(' ', '_')
            else:
                prefix = ""
            suffix = "%s_%s_%s" % (bench_data[context]['cpu_str'].replace(' ', '_'),
                                   input_dataset.replace('/', '-'),
                                   args.ptime)
            save_figures(figs=[plt.figure(fi),], output_dir=args.fd, overwrite=not args.nc,
                         file_format=args.ff, prefix=prefix, suffix=suffix,
                         fig_size=(args.fw, args.fh))
            fi += 1

def main():
    args = parse_args()
    _process_args(args)
    files = get_files(args.dir, args.f, args.r)
    bench_data = read_files(files, args.verbose)
    process_bench_data(bench_data, args.si, args.verbose)
    if args.p or args.fo:
        import matplotlib.pyplot as plt
        plot_bench_data(bench_data, args.ptitle, args.psort, args.ptime, args.si, args.fw, args.fh)
    if args.fo:
        save_bench_data(bench_data, args)
    if args.p:
        plt.show()

if __name__ == '__main__':
    main()

#!/bin/sh

# SPDX-License-Identifier: MIT
# Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

SCRIPT_DIR="$(cd "$(dirname "$0")"; pwd -P)"
TOP_DIR="${SCRIPT_DIR}/.."

DEFAULT_CXX=g++
DEFAULT_CXX_STD=17
DEFAULT_BUILD_DIR="${TOP_DIR}/build"
DEFAULT_OUTPUT_DIR="${TOP_DIR}/benchmarks/results/${DEFAULT_CXX}-${DEFAULT_CXX_STD}"
DEFAULT_NUM_TRIALS=1
DEFAULT_FILE_PREFIX="benchmarks-"

function print_usage ()
{
    echo "Usage: "
    echo "${BASH_SOURCE} [--help] <options> <cmake args...>"
    echo ""
    echo "Options"
    echo "  -h,--help"
    echo "    Prints help"
    echo "  -b,--build-dir <dir>"
    echo "    Build directory for library and benchmarks"
    echo "  -c,--cxx <compiler>"
    echo "    C++ compiler to use when building library and benchmarks"
    echo "  -n,--num-trials"
    echo "    Number of benchmark trials or repetitions to run"
    echo "  -p,--prefix"
    echo "    Benchmark output file prefix"
    echo "  -o,--output-dir <dir>"
    echo "    Benchmark results output directory"
    echo "  -s,--cxx-std <c++-std>"
    echo "    C++ standard to use when compiling library and benchmarks"
    echo ""
    echo "cmake args"
    echo "  Additional arguments passed to CMake for build"
}

function info ()
{
    echo "$*" 1>&2
}

function error ()
{
    info "$*"
    exit 1
}

function check_return ()
{
    if [ "$?" -ne "0" ]; then
        exit 1
    fi
}

# run command
function run ()
{
    info "+ $@"
    "$@"
    check_return
}

# run_silent command
function run_silent ()
{
    info "+ $@"
    "$@" > /dev/null 2>&1
    check_return
}

# have_command command
function have_command ()
{
    command -v "$1" > /dev/null 2>&1
}

# is_empty dir
function is_empty ()
{
    if [ -z "$(ls -A $1)" ]; then
        return 0
    else
        return 1
    fi
}

# build_benchmarks build_dir cxx cxx_std <cmake_args...>
function build_benchmarks ()
{
    local build_dir="$1"; shift
    local cxx="$1"; shift
    local cxx_std="$1"; shift
    local cmake_args=("$@")

    if [ "${cxx}" == clang++ ]; then
        local cc=clang
    else
        local cc=gcc
    fi
    if [ ! -d "${build_dir}" ]; then
        run mkdir -p "${build_dir}"
    else
        info "'${build_dir}' already exists. Continuing build."
    fi
    if have_command ninja; then
        local generator="Ninja"
        local build_cmd=("ninja")
    else
        local generator="Unix Makefiles"
        local build_cmd=("make" "-j$(nproc)")
    fi

    local owd="$PWD"
    run cd "${build_dir}"
    run cmake -G "${generator}" \
        -DCMAKE_C_COMPILER="${cc}" \
        -DCMAKE_CXX_COMPILER="${cxx}" \
        -DCMAKE_CXX_STANDARD="${cxx_std}" \
        -DCMAKE_BUILD_TYPE=RELEASE \
        "${cmake_args[@]}" \
        "${TOP_DIR}"
    run "${build_cmd[@]}" benchmarks
    run cd "${owd}"
}

# run_benchmarks build_dir output_dir file_prefix num_trials
function run_benchmarks()
{
    local build_dir="$1"
    local output_dir="$2"
    local file_prefix="$3"
    local num_trials="$4"

    if [ ! -d "${output_dir}" ]; then
        run mkdir -p "${output_dir}"
    else
        info "'${output_dir}' already exists. Continuing benchmarks."
    fi

    output_dir="$(cd "${output_dir}"; pwd -P)"
    local outfile="${output_dir}/${file_prefix}$(date -Is)"
    i=0
    while [ -f "${outfile}" ]; do
        try_outfile="${outfile}-${i}"
        info "'${outfile}' already exists. Trying '${try_outfile}'."
        outfile="${try_outfile}"
        i=$((${i} + 1))
    done

    local owd="$PWD"
    run cd "${build_dir}"
    run ./benchmarks/benchmarks \
        --benchmark_report_aggregates_only=false \
        --benchmark_repetitions="${num_trials}" \
        --benchmark_out_format=json \
        --benchmark_out="${outfile}"
    run cd "${owd}"
}

positional=()
while [ $# -gt 0 ]; do
    case "$1" in
        -h|--help)
            print_usage
            exit 0
            ;;
        -b|--build-dir)
            build_dir="$2"
            shift 2
            ;;
        -c|--cxx)
            cxx="$2"
            shift 2
            ;;
        -n|--num-trials)
            num_trials="$2"
            shift 2
            ;;
        -o|--output-dir)
            output_dir="$2"
            shift 2
            ;;
        -p|--prefix)
            file_prefix="$2"
            shift 2
            ;;
        -s|--cxx-std)
            cxx_std="$2"
            shift 2
            ;;
        *)
            positional+=("$1")
            shift
            ;;
    esac
done
set -- "${positional[@]}" # restore positional parameters

cxx="${cxx:-${DEFAULT_CXX}}"
cxx_std="${cxx_std:-${DEFAULT_CXX_STD}}"
build_dir="${build_dir:-${DEFAULT_BUILD_DIR}}"
output_dir="${output_dir:-${DEFAULT_OUTPUT_DIR}}"
num_trials="${num_trials:-${DEFAULT_NUM_TRIALS}}"
file_prefix="${file_prefix:-${DEFAULT_FILE_PREFIX}}"

cmake_args=("$@")

build_benchmarks "${build_dir}" "${cxx}" "${cxx_std}" "${cmake_args[@]}"
run_benchmarks "${build_dir}" "${output_dir}" "${file_prefix}" "${num_trials}"

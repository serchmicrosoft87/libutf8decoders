// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#ifndef LIBUTF8DECODERS_UTF16_HPP
#define LIBUTF8DECODERS_UTF16_HPP

#include "libutf8decoders/config.hpp"
#include "libutf8decoders/features.hpp"

#include "libutf8decoders/api.hpp"
#include "libutf8decoders/types.hpp"
#include "libutf8decoders/unicode.hpp"

namespace utf8decoders {
namespace utf16 {

using ::utf8decoders::size_t;
using ::utf8decoders::ptrdiff_t;
using ::utf8decoders::u16_char_t;
using ::utf8decoders::u32_char_t;
using ::utf8decoders::u16_s_t;
using ::utf8decoders::u32_s_t;
#ifdef U8D_USE_STD_STRING_VIEW
using ::utf8decoders::u16_sv_t;
#else
using ::utf8decoders::u16_char_t;
#endif

using ::utf8decoders::unicode::SURROGATE_BASE;
using ::utf8decoders::unicode::BMP_PRIVATE_BASE;
using ::utf8decoders::unicode::SMP_1_BASE;

static U8D_CONSTEXPR_DECL const u32_char_t LOW_10 = 0x3FF;
static U8D_CONSTEXPR_DECL const u32_char_t LOW_OFFSET = SURROGATE_BASE + LOW_10 + 1; // 0xDC00

static U8D_CONSTEXPR_DECL const u32_char_t HIGH_OFFSET =
    SURROGATE_BASE - (SMP_1_BASE >> 10); // D7C0
static U8D_CONSTEXPR_DECL const u32_char_t SURROGATE_OFFSET =
    SMP_1_BASE - (SURROGATE_BASE << 10) - LOW_OFFSET;

U8D_CONSTEXPR u32_char_t
decode_surrogates(u16_char_t low, u16_char_t high) noexcept {
    return (static_cast<u32_char_t>(high) << 10) + low + SURROGATE_OFFSET;
}

U8D_CONSTEXPR void
encode_surrogates(u32_char_t code_point, u16_char_t& low, u16_char_t& high) noexcept {
    low = static_cast<u16_char_t>(LOW_OFFSET + (code_point & LOW_10));
    high = static_cast<u16_char_t>(HIGH_OFFSET + (code_point >> 10));
}

size_t
encode_next(u32_char_t src, u16_s_t& dst, size_t dst_offset = 0) noexcept;

#ifdef U8D_USE_STD_STRING_VIEW

U8D_CONSTEXPR ptrdiff_t
decode_next(u16_sv_t src, u32_char_t& code_point) noexcept {
    using InputChar = u16_sv_t::value_type;
    using InputIt = u16_sv_t::const_pointer;
    using size_t = u16_sv_t::size_type;

    InputIt src_it = &*src.cbegin();
    size_t src_size = src.size();

    if (src_size == 0)
        return 0;

    InputChar code_unit = *src_it++;

    if (code_unit < SURROGATE_BASE || code_unit >= BMP_PRIVATE_BASE) {
        code_point = code_unit;
        return 1;
    }

    // expect surrogate pair: high and then low
    if (code_unit < LOW_OFFSET && src_size > 1) {
        InputChar high = code_unit;
        InputChar low = *src_it;
        if (low >= LOW_OFFSET && low < BMP_PRIVATE_BASE) {
            code_point = decode_surrogates(low, high);
            return 2;
        }
    }

    return static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::ERROR);
}

ptrdiff_t
decode(u16_sv_t src, u32_s_t& dst, size_t dst_offset = 0) noexcept;

size_t
encode(u32_sv_t src, u16_s_t& dst, size_t dst_offset = 0) noexcept;

#else /* U8D_USE_STD_STRING_VIEW */

U8D_CONSTEXPR ptrdiff_t
decode_next(const u16_char_t* src, size_t count, u32_char_t& code_point) noexcept {
    using InputChar = u16_char_t;
    using InputIt = const u16_char_t*;

    InputIt src_it = src;
    size_t src_size = count;

    if (src_size == 0)
        return 0;

    InputChar code_unit = *src_it++;

    if (code_unit < SURROGATE_BASE || code_unit >= BMP_PRIVATE_BASE) {
        code_point = code_unit;
        return 1;
    }

    // expect surrogate pair: high and then low
    if (code_unit < LOW_OFFSET && src_size > 1) {
        InputChar high = code_unit;
        InputChar low = *src_it;
        if (low >= LOW_OFFSET && low < BMP_PRIVATE_BASE) {
            code_point = decode_surrogates(low, high);
            return 2;
        }
    }

    return static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::ERROR);
}

ptrdiff_t
decode(const u16_char_t* src, size_t count, u32_s_t& dst, size_t dst_offset = 0) noexcept;

size_t
encode(const u32_char_t* src, size_t count, u16_s_t& dst, size_t dst_offset = 0) noexcept;

#endif /* U8D_USE_STD_STRING_VIEW */

} // namespace utf16
} // namespace utf8decoders

#endif /* LIBUTF8DECODERS_UTF16_HPP */

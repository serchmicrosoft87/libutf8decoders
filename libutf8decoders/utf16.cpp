// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#include "libutf8decoders/utf16.hpp"

#include <cstdint>

namespace utf8decoders {
namespace utf16 {

size_t
encode_next(u32_char_t src, u16_s_t& dst, size_t dst_offset) noexcept {
    using OutputChar = u16_s_t::value_type;
    using OutputIt = u16_s_t::pointer;

    OutputIt dst_it = &*dst.begin() + dst_offset;

    if (src < SMP_1_BASE) {
        *dst_it++ = static_cast<u16_char_t>(src);
        return 1;
    }

    OutputChar low{}, high{};

    encode_surrogates(src, low, high);
    *dst_it++ = high;
    *dst_it = low;

    return 2;
}

#ifdef U8D_USE_STD_STRING_VIEW

ptrdiff_t
decode(u16_sv_t src, u32_s_t& dst, size_t dst_offset) noexcept {
    using InputIt = u16_sv_t::const_pointer;
    using OutputIt = u32_s_t::pointer;

    InputIt src_it = &*src.cbegin();
    InputIt src_end = src_it + src.size();
    size_t src_units = 0;

    OutputIt dst_begin = &*dst.begin() + dst_offset;
    OutputIt dst_it = dst_begin;

    u32_char_t code_point = 0;

    while (src_it < src_end) {
        if ((src_units =
             static_cast<size_t>(
                 decode_next({src_it, static_cast<size_t>(src_end - src_it)}, code_point))) !=
            static_cast<size_t>(DECODE_NEXT_STATUS::ERROR)) {
            src_it += src_units;
            *dst_it++ = code_point;
        } else {
            return static_cast<ptrdiff_t>(DECODE_STATUS::ERROR);
        }
    }
    return dst_it - dst_begin;
}

size_t
encode(u32_sv_t src, u16_s_t& dst, size_t dst_offset) noexcept {
    using InputIt = u32_sv_t::const_pointer;

    InputIt src_it = &*src.cbegin();
    InputIt src_end = src_it + src.size();

    size_t code_units = 0;

    for (; src_it < src_end; ++src_it) {
        code_units += encode_next(*src_it, dst, dst_offset + code_units);
    }
    return code_units;
}

#else /* U8D_USE_STD_STRING_VIEW */

ptrdiff_t
decode(const u16_char_t* src, size_t count,
       u32_s_t& dst, size_t dst_offset) noexcept {
    using InputIt = const u16_char_t*;
    using OutputIt = u32_s_t::pointer;

    InputIt src_it = src;
    InputIt src_end = src_it + count;
    size_t src_units = 0;

    OutputIt dst_begin = &*dst.begin() + dst_offset;
    OutputIt dst_it = dst_begin;

    u32_char_t code_point = 0;

    while (src_it < src_end) {
        if ((src_units =
             static_cast<size_t>(
                 decode_next(src_it, static_cast<size_t>(src_end - src_it), code_point))) !=
            static_cast<size_t>(DECODE_NEXT_STATUS::ERROR)) {
            src_it += src_units;
            *dst_it++ = code_point;
        } else {
            return static_cast<ptrdiff_t>(DECODE_STATUS::ERROR);
        }
    }
    return dst_it - dst_begin;
}

size_t
encode(const u32_char_t* src, size_t count,
       u16_s_t& dst, size_t dst_offset) noexcept {
    using InputIt = const u32_char_t*;

    InputIt src_it = src;
    InputIt src_end = src_it + count;

    size_t code_units = 0;

    for (; src_it < src_end; ++src_it) {
        code_units += encode_next(*src_it, dst, dst_offset + code_units);
    }
    return code_units;
}

#endif /* U8D_USE_STD_STRING_VIEW */

} // namespace utf16
} // namespace utf8decoders

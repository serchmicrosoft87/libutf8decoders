// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#ifndef LIBUTF8DECODERS_UNICODE_HPP
#define LIBUTF8DECODERS_UNICODE_HPP

#include "libutf8decoders/config.hpp"
#include "libutf8decoders/features.hpp"

#include "libutf8decoders/types.hpp"

namespace utf8decoders {
namespace unicode {

static U8D_CONSTEXPR_DECL const u32_char_t SURROGATE_BASE = 0xD800;
static U8D_CONSTEXPR_DECL const u32_char_t BMP_PRIVATE_BASE = 0xE000;
static U8D_CONSTEXPR_DECL const u32_char_t BMP_RESERVED_NONCHARACTER_BASE = 0xFDD0;
static U8D_CONSTEXPR_DECL const u32_char_t BMP_RESERVED_NONCHARACTER_END = 0xFDF0;
static U8D_CONSTEXPR_DECL const u32_char_t REPLACEMENT_CHARACTER = 0xFFFD;
static U8D_CONSTEXPR_DECL const u32_char_t SMP_1_BASE = 0x10000;
static U8D_CONSTEXPR_DECL const u32_char_t SPUA_A_BASE = 0xF0000;
static U8D_CONSTEXPR_DECL const u32_char_t SPUA_B_BASE = 0x100000;
static U8D_CONSTEXPR_DECL const u32_char_t SPUA_B_END = 0x110000;

} // namespace unicode
} // namespace utf8decoders

#endif /* LIBUTF8DECODERS_UNICODE_HPP */

// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#ifndef LIBUTF8DECODERS_BH_UTF8DECODERS_BH_HPP
#define LIBUTF8DECODERS_BH_UTF8DECODERS_BH_HPP

#include "libutf8decoders/config.hpp"
#include "libutf8decoders/features.hpp"

#include <cstdint>

#include "libutf8decoders/api.hpp"
#include "libutf8decoders/bh/bh.hpp"
#include "libutf8decoders/types.hpp"

namespace utf8decoders {
namespace bh {

using ::utf8decoders::size_t;
using ::utf8decoders::ptrdiff_t;
using ::utf8decoders::u32_char_t;
using ::utf8decoders::u32_s_t;
#ifdef U8D_USE_STD_STRING_VIEW
using ::utf8decoders::u8_sv_t;
#else
using ::utf8decoders::u8_char_t;
#endif

using ::utf8decoders::DECODE_NEXT_STATUS;
using ::utf8decoders::DECODE_STATUS;

using decode_next_stateful_f = ::std::uint8_t (*)(::std::uint8_t byte,
                                                  u32_char_t& code_point,
                                                  ::std::uint8_t& state) U8D_NOEXCEPT_TYPE_SPEC;

#ifdef U8D_USE_STD_STRING_VIEW

template<decode_next_stateful_f dns>
U8D_CONSTEXPR ptrdiff_t
decode_next(u8_sv_t src, u32_char_t& code_point) noexcept {
    using InputIt = const ::std::uint8_t*;

    InputIt src_it = &*src.cbegin();
    InputIt src_end = src_it + src.size();

    ::std::uint8_t state = UTF8_ACCEPT;
    code_point = 0;

    for (ptrdiff_t code_units = 1; src_it < src_end; ++code_units) {
        if (!dns(*src_it++, code_point, state)) {
            return code_units;
        }
    }

    return state == UTF8_ACCEPT ?
        static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::OK) :
        static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::ERROR);
}

template<decode_next_stateful_f dns>
ptrdiff_t
decode(u8_sv_t src, u32_s_t& dst, size_t dst_offset = 0) noexcept {
    using InputIt = const ::std::uint8_t*;
    using OutputIt = u32_s_t::pointer;

    InputIt src_it = &*src.cbegin();
    InputIt src_end = src_it + src.size();

    OutputIt dst_begin = &*dst.begin() + dst_offset;
    OutputIt dst_it = dst_begin;

    ::std::uint8_t state = UTF8_ACCEPT;
    u32_char_t code_point = 0;

    for (; src_it < src_end; ++src_it) {
        if (!dns(*src_it, code_point, state)) {
            *dst_it++ = code_point;
        }
    }

    return state == UTF8_ACCEPT ? dst_it - dst_begin :
        static_cast<ptrdiff_t>(DECODE_STATUS::ERROR);
}

#else /* U8D_USE_STD_STRING_VIEW */

template<decode_next_stateful_f dns>
U8D_CONSTEXPR ptrdiff_t
decode_next(const u8_char_t* src, size_t count, u32_char_t& code_point) noexcept {
    using InputIt = const ::std::uint8_t*;

    InputIt src_it = src;
    InputIt src_end = src + count;

    ::std::uint8_t state = UTF8_ACCEPT;
    code_point = 0;

    for (ptrdiff_t code_units = 1; src_it < src_end; ++code_units) {
        if (!dns(*src_it++, code_point, state)) {
            return code_units;
        }
    }

    return state == UTF8_ACCEPT ?
        static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::OK) :
        static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::ERROR);
}

template<decode_next_stateful_f dns>
ptrdiff_t
decode(const u8_char_t* src, size_t count, u32_s_t& dst, size_t dst_offset = 0) noexcept {
    using InputIt = const ::std::uint8_t*;
    using OutputIt = u32_s_t::pointer;

    InputIt src_it = src;
    InputIt src_end = src + count;

    OutputIt dst_begin = &*dst.begin() + dst_offset;
    OutputIt dst_it = dst_begin;

    ::std::uint8_t state = UTF8_ACCEPT;
    u32_char_t code_point = 0;

    for (; src_it < src_end; ++src_it) {
        if (!dns(*src_it, code_point, state)) {
            *dst_it++ = code_point;
        }
    }

    return state == UTF8_ACCEPT ? dst_it - dst_begin :
        static_cast<ptrdiff_t>(DECODE_STATUS::ERROR);
}

#endif /* U8D_USE_STD_STRING_VIEW */

} // namespace bh
} // namespace utf8decoders

#endif /* LIBUTF8DECODERS_BH_UTF8DECODERS_BH_HPP */

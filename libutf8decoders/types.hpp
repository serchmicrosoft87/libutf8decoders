// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#ifndef LIBUTF8DECODERS_TYPES_HPP
#define LIBUTF8DECODERS_TYPES_HPP

#include "libutf8decoders/config.hpp"
#include "libutf8decoders/features.hpp"

#include <cstddef>
#if !defined(U8D_HAVE_CHAR8_TYPE) || !defined(U8D_HAVE_LIB_CHAR8_TYPE)
#  include <cstdint>
#endif
#include <string>
#ifdef U8D_USE_STD_STRING_VIEW
#  include <string_view>
#endif

namespace utf8decoders {

using size_t = ::std::size_t;
using ptrdiff_t = ::std::ptrdiff_t;

#ifdef U8D_HAVE_CHAR8_TYPE
using u8_char_t = char8_t;
#else
using u8_char_t = ::std::uint8_t;
#endif

using u16_char_t = char16_t;
using u32_char_t = char32_t;

#ifdef U8D_HAVE_LIB_CHAR8_TYPE
using u8_s_t = ::std::u8string;
#else
using u8_s_t = ::std::basic_string<::std::uint8_t>;
#endif
using u16_s_t = ::std::u16string;
using u32_s_t = ::std::u32string;

#ifdef U8D_USE_STD_STRING_VIEW

#ifdef U8D_HAVE_LIB_CHAR8_TYPE
using u8_sv_t = ::std::u8string_view;
#else
using u8_sv_t = ::std::basic_string_view<::std::uint8_t>;
#endif

using u16_sv_t = ::std::u16string_view;
using u32_sv_t = ::std::u32string_view;

#endif /* U8D_USE_STD_STRING_VIEW */

#ifdef U8D_USE_STD_STRING_VIEW

using decode_next_f = ptrdiff_t (*)(u8_sv_t src,
                                    u32_char_t& code_point) U8D_NOEXCEPT_TYPE_SPEC;

using decode_f = ptrdiff_t (*)(u8_sv_t src,
                               u32_s_t& dst,
                               size_t dst_offset);

#else

using decode_next_f = ptrdiff_t (*)(const u8_char_t* src,
                                    size_t count,
                                    u32_char_t& code_point) U8D_NOEXCEPT_TYPE_SPEC;

using decode_f = ptrdiff_t (*)(const u8_char_t* src,
                               size_t count,
                               u32_s_t& dst,
                               size_t dst_offset);

#endif

} // namespace utf8decoders

#endif /* LIBUTF8DECODERS_TYPES_HPP */

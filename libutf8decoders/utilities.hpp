// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#ifndef LIBUTF8DECODERS_UTILITIES_HPP
#define LIBUTF8DECODERS_UTILITIES_HPP

#include <string>
#ifdef U8D_USE_STD_STRING_VIEW
#include <string_view>
#endif

#include "libutf8decoders/types.hpp"

namespace utf8decoders {
namespace utilities {

inline u8_s_t
to_u8_s(const ::std::string& s) noexcept {
    return u8_s_t(s.begin(), s.end());
}

template<size_t N>
inline u8_s_t
to_u8_s(const char (&s)[N]) noexcept {
    return u8_s_t(s, s+N);
}

inline ::std::string
to_s(const u8_s_t& s) noexcept {
    return ::std::string(s.begin(), s.end());
}

inline const u8_char_t*
to_u8_cs(const char* s) noexcept {
    return reinterpret_cast<const u8_char_t*>(s);
}

template<size_t N>
inline const u8_char_t*
to_u8_cs(const char (&s)[N]) noexcept {
    return reinterpret_cast<const u8_char_t*>(s);
}

inline const char*
to_cs(const u8_char_t* s) noexcept {
    return reinterpret_cast<const char*>(s);
}

#ifdef U8D_USE_STD_STRING_VIEW

inline u8_sv_t
to_u8_sv(::std::string_view sv) noexcept {
    return u8_sv_t(
        reinterpret_cast<u8_sv_t::const_pointer>(&*sv.cbegin()), sv.size());
}

template<size_t N>
inline u8_sv_t
to_u8_sv(const char (&s)[N]) noexcept {
    return u8_sv_t(reinterpret_cast<u8_sv_t::const_pointer>(s), N);
}

inline ::std::string_view
to_sv(u8_sv_t sv) noexcept {
    return ::std::string_view(
        reinterpret_cast<::std::string_view::const_pointer>(&*sv.cbegin()), sv.size());
}

#endif

} // namespace utilities
} // namespace utf8decoders

#endif /* LIBUTF8DECODERS_UTILITIES_HPP */

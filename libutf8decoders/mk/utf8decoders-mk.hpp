// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#ifndef LIBUTF8DECODERS_MK_UTF8DECODERS_MK_HPP
#define LIBUTF8DECODERS_MK_UTF8DECODERS_MK_HPP

#include "libutf8decoders/config.hpp"
#include "libutf8decoders/features.hpp"

#include "libutf8decoders/api.hpp"
#include "libutf8decoders/mk/icu.hpp"
#include "libutf8decoders/types.hpp"

namespace utf8decoders {
namespace mk {

using ::utf8decoders::size_t;
using ::utf8decoders::ptrdiff_t;
using ::utf8decoders::u32_char_t;
#ifdef U8D_USE_STD_STRING_VIEW
using ::utf8decoders::u8_sv_t;
#endif
using ::utf8decoders::u8_char_t;

using ::utf8decoders::DECODE_NEXT_STATUS;

using decode_next_f = ptrdiff_t (*)(const u8_char_t* src,
                                    size_t count,
                                    u32_char_t& code_point) U8D_NOEXCEPT_TYPE_SPEC;

#ifdef U8D_USE_STD_STRING_VIEW

template<decode_next_f dn>
U8D_CONSTEXPR ptrdiff_t
decode_next(u8_sv_t src, u32_char_t& code_point) noexcept {
    using InputIt = const u8_char_t*;

    InputIt src_begin = &*src.cbegin();
    size_t src_size = src.size();

    return dn(src_begin, src_size, code_point);
}

#else /* U8D_USE_STD_STRING_VIEW */

template<decode_next_f dn>
U8D_CONSTEXPR ptrdiff_t
decode_next(const u8_char_t* src, size_t count, u32_char_t& code_point) noexcept {
    using InputIt = const u8_char_t*;

    InputIt src_begin = src;
    size_t src_size = count;

    return dn(src_begin, src_size, code_point);
}

#endif /* U8D_USE_STD_STRING_VIEW */

} // namespace mk
} // namespace utf8decoders

#endif /* LIBUTF8DECODERS_MK_UTF8DECODERS_MK_HPP */

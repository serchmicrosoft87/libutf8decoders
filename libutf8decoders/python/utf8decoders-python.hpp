// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#ifndef LIBUTF8DECODERS_PYTHON_UTF8DECODERS_PYTHON_HPP
#define LIBUTF8DECODERS_PYTHON_UTF8DECODERS_PYTHON_HPP

#include "libutf8decoders/config.hpp"

#include "libutf8decoders/types.hpp"

namespace utf8decoders {
namespace python {

using ::utf8decoders::size_t;
using ::utf8decoders::ptrdiff_t;
using ::utf8decoders::u32_char_t;
using ::utf8decoders::u32_s_t;
#ifdef U8D_USE_STD_STRING_VIEW
using ::utf8decoders::u8_sv_t;
#else
using ::utf8decoders::u8_char_t;
#endif

#ifdef U8D_USE_STD_STRING_VIEW

ptrdiff_t
decode(u8_sv_t src, u32_s_t& dst, size_t dst_offset = 0) noexcept;

#else /* U8D_USE_STD_STRING_VIEW */

ptrdiff_t
decode(const u8_char_t* src, size_t count, u32_s_t& dst, size_t dst_offset = 0) noexcept;

#endif /* U8D_USE_STD_STRING_VIEW */

} // namespace python
} // namespace utf8decoders

#endif /* LIBUTF8DECODERS_PYTHON_UTF8DECODERS_PYTHON_HPP */

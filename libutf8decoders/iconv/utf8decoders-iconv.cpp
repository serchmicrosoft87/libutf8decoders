// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#include "libutf8decoders/iconv/utf8decoders-iconv.hpp"

#include <cstddef>

#include "libutf8decoders/api.hpp"
#include "libutf8decoders/iconv/iconv.hpp"

namespace utf8decoders {
namespace iconv {

using ::utf8decoders::DECODE_NEXT_STATUS;
using ::utf8decoders::DECODE_STATUS;

static const iconv ic{};

#ifdef U8D_USE_STD_STRING_VIEW

ptrdiff_t
decode_next(u8_sv_t src, u32_char_t& code_point) noexcept {
    using InputIt = char*;
    using OutputIt = char*;
    using size_t = ::std::size_t;

    InputIt src_begin = reinterpret_cast<InputIt>(const_cast<u8_char_t*>(&*src.cbegin()));
    InputIt src_it = src_begin;
    size_t src_bytes = src.size();
    OutputIt dst_it = reinterpret_cast<OutputIt>(&code_point);
    size_t dst_bytes = 1*sizeof(u32_char_t);

    return ic.convert(src_it, src_bytes, dst_it, dst_bytes) != static_cast<size_t>(-1) ?
        src_it - src_begin :
        static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::ERROR);
}

ptrdiff_t
decode(u8_sv_t src, u32_s_t& dst, size_t dst_offset) noexcept {
    using InputIt = char*;
    using OutputIt = char*;
    using size_t = ::std::size_t;

    InputIt src_it = reinterpret_cast<InputIt>(const_cast<u8_char_t*>(&*src.cbegin()));
    size_t src_bytes = src.size();
    OutputIt dst_begin = reinterpret_cast<OutputIt>(&*dst.begin()) + dst_offset;
    OutputIt dst_it = dst_begin;
    size_t dst_bytes = dst.size()*sizeof(u32_s_t::value_type);

    return ic.convert(src_it, src_bytes, dst_it, dst_bytes) != static_cast<size_t>(-1) ?
        reinterpret_cast<u32_s_t::const_pointer>(dst_it) -
        reinterpret_cast<u32_s_t::const_pointer>(dst_begin) :
        static_cast<ptrdiff_t>(DECODE_STATUS::ERROR);
}

#else /* U8D_USE_STD_STRING_VIEW */

ptrdiff_t
decode_next(const u8_char_t* src, size_t count, u32_char_t& code_point) noexcept {
    using InputIt = char*;
    using OutputIt = char*;
    using size_t = ::std::size_t;

    InputIt src_begin = reinterpret_cast<InputIt>(const_cast<u8_char_t*>(src));
    InputIt src_it = src_begin;
    size_t src_bytes = count;
    OutputIt dst_it = reinterpret_cast<OutputIt>(&code_point);
    size_t dst_bytes = 1*sizeof(u32_char_t);

    return ic.convert(src_it, src_bytes, dst_it, dst_bytes) != static_cast<size_t>(-1) ?
        src_it - src_begin :
        static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::ERROR);
}

ptrdiff_t
decode(const u8_char_t* src, size_t count, u32_s_t& dst, size_t dst_offset) noexcept {
    using InputIt = char*;
    using OutputIt = char*;
    using size_t = ::std::size_t;

    InputIt src_it = reinterpret_cast<InputIt>(const_cast<u8_char_t*>(src));
    size_t src_bytes = count;
    OutputIt dst_begin = reinterpret_cast<OutputIt>(&*dst.begin()) + dst_offset;
    OutputIt dst_it = dst_begin;
    size_t dst_bytes = dst.size()*sizeof(u32_s_t::value_type);

    return ic.convert(src_it, src_bytes, dst_it, dst_bytes) != static_cast<size_t>(-1) ?
        reinterpret_cast<u32_s_t::const_pointer>(dst_it) -
        reinterpret_cast<u32_s_t::const_pointer>(dst_begin) :
        static_cast<ptrdiff_t>(DECODE_STATUS::ERROR);
}

#endif /* U8D_USE_STD_STRING_VIEW */

} // namespace iconv
} // namespace utf8decoders

// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#include "libutf8decoders/iconv/iconv.hpp"

#include <cerrno>
#include <stdexcept>
#include <string>

namespace utf8decoders {
namespace iconv {

iconv::iconv(const char* from_encoding,
             const char* to_encoding) :
    cd_{::iconv_open(to_encoding, from_encoding), &::iconv_close} {
    if (cd_.get() == reinterpret_cast<::iconv_t>(-1)) {
        if (errno == EINVAL) {
            throw ::std::runtime_error(::std::string("Conversion from ") + from_encoding +
                                       " to " + to_encoding + " not supported.");
        } else {
            throw ::std::runtime_error("Unknown iconv_open() error");
        }
    }
}

iconv::iconv(const ::std::string& from_encoding,
             const ::std::string& to_encoding) :
    iconv(from_encoding.c_str(), to_encoding.c_str()) {}

iconv::size_t
iconv::convert(InputIt& src_begin, size_t& src_bytes,
               OutputIt& dst_begin, size_t& dst_bytes) const noexcept {
    return ::iconv(cd_.get(), &src_begin, &src_bytes, &dst_begin, &dst_bytes);
}

void
iconv::convert_with_check(InputIt& src_begin, size_t& src_bytes,
                          OutputIt& dst_begin, size_t& dst_bytes) const {
    if (convert(src_begin, src_bytes, dst_begin, dst_bytes) == static_cast<size_t>(-1)) {
        if (errno == EINVAL) {
            throw ::std::runtime_error("Incomplete multibyte sequence in the input");
        } else if (errno == EILSEQ) {
            throw ::std::runtime_error("Invalid multibyte sequence in the input");
        } else if (errno == E2BIG) {
            throw ::std::runtime_error("Insufficient space in output buffer");
        } else {
            throw ::std::runtime_error("Unknown iconv() error");
        }
    }
}

void
iconv::reset() const noexcept {
    ::iconv(cd_.get(), nullptr, nullptr, nullptr, nullptr);
}

} // namespace iconv
} // namespace utf8decoders

// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#ifndef LIBUTF8DECODERS_ICONV_ICONV_HPP
#define LIBUTF8DECODERS_ICONV_ICONV_HPP

#include <cstddef>
#include <memory>
#include <string>

#include <iconv.h>

namespace utf8decoders {
namespace iconv {

class iconv {
public:
    using iconv_t = ::std::unique_ptr<::std::remove_pointer<::iconv_t>::type,
                                       decltype(&::iconv_close)>;
    using InputIt = char*;
    using OutputIt = char*;
    using size_t = ::std::size_t;

    static constexpr const char* default_from_encoding = "UTF-8";
    static constexpr const char* default_to_encoding = "UTF-32LE";

    iconv(const char* from_encoding = default_from_encoding,
          const char* to_encoding = default_to_encoding);
    explicit iconv(const ::std::string& from_encoding,
                   const ::std::string& to_encoding);

    size_t
    convert(InputIt& src_begin, size_t& src_bytes,
            OutputIt& dst_begin, size_t& dst_bytes) const noexcept;

    void
    convert_with_check(InputIt& src_begin, size_t& src_bytes,
                       OutputIt& dst_begin, size_t& dst_bytes) const;

    void
    reset() const noexcept;

private:
    iconv_t cd_;
};

} // namespace iconv
} // namespace utf8decoders

#endif /* LIBUTF8DECODERS_ICONV_ICONV_HPP */

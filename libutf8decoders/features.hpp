// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#ifndef LIBUTF8DECODERS_FEATURES_HPP
#define LIBUTF8DECODERS_FEATURES_HPP

#ifdef __has_feature
#  define U8D_HAS_FEATURE(x) __has_feature(x)
#else
#  define U8D_HAS_FEATURE(x) 0
#endif

#if defined(__GNUC__) && !defined(__clang__)
#  define U8D_GCC_VERSION (__GNUC__ * 100 + __GNUC_MINOR__)
#else
#  define U8D_GCC_VERSION 0
#endif

#ifdef _MSC_VER
#  define U8D_MSC_VER _MSC_VER
#else
#  define U8D_MSC_VER 0
#endif

// Check if relaxed C++14 constexpr is supported.
// GCC doesn't allow throw in constexpr until version 6 (bug 67371).
#ifndef U8D_USE_CONSTEXPR
#  define U8D_USE_CONSTEXPR                                           \
    (U8D_HAS_FEATURE(cxx_relaxed_constexpr) || U8D_MSC_VER >= 1910 || \
     (U8D_GCC_VERSION >= 600 && __cplusplus >= 201402L))
#endif
#if U8D_USE_CONSTEXPR
#  define U8D_CONSTEXPR constexpr
#  define U8D_CONSTEXPR_DECL constexpr
#else
#  define U8D_CONSTEXPR inline
#  define U8D_CONSTEXPR_DECL
#endif

#ifndef U8D_USE_NOEXCEPT_TYPE_SPEC
#  define U8D_USE_NOEXCEPT_TYPE_SPEC         \
     __cpp_noexcept_function_type == 201510L
#endif
#if U8D_USE_NOEXCEPT_TYPE_SPEC
#  define U8D_NOEXCEPT_TYPE_SPEC noexcept
#else
#  define U8D_NOEXCEPT_TYPE_SPEC
#endif

#if defined(__cpp_char8_t) && __cpp_char8_t == 201811L
#  define U8D_HAVE_CHAR8_TYPE
#endif

#if defined(__cpp_lib_char8_t) && __cpp_lib_char8_t == 201811L
#  define U8D_HAVE_LIB_CHAR8_TYPE
#endif

#endif /* LIBUTF8DECODERS_FEATURES_HPP */

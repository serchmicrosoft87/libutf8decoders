// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#include "libutf8decoders/llvm/utf8decoders-llvm.hpp"

#include "llvm/Support/ConvertUTF.h"

#include "libutf8decoders/api.hpp"

namespace utf8decoders {
namespace llvm {

using ::utf8decoders::DECODE_NEXT_STATUS;
using ::utf8decoders::DECODE_STATUS;

#ifdef U8D_USE_STD_STRING_VIEW

ptrdiff_t
decode_next(u8_sv_t src, u32_char_t& code_point) noexcept {
    using InputIt = const ::llvm::UTF8*;
    using OutputIt = ::llvm::UTF32*;

    InputIt src_begin = &*src.cbegin();
    InputIt src_it = src_begin;
    InputIt src_end = &*src.cend();

    OutputIt dst_begin = reinterpret_cast<OutputIt>(&code_point);
    OutputIt dst_end = dst_begin + 1;

    return ::llvm::ConvertUTF8toUTF32(&src_it, src_end, &dst_begin, dst_end,
                                      ::llvm::ConversionFlags::strictConversion) ==
        ::llvm::ConversionResult::conversionOK ?
        src_it - src_begin :
        static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::ERROR);
}

ptrdiff_t
decode(u8_sv_t src, u32_s_t& dst, size_t dst_offset) noexcept {
    using InputIt = const ::llvm::UTF8*;
    using OutputIt = ::llvm::UTF32*;

    InputIt src_begin = &*src.cbegin();
    InputIt src_it = src_begin;
    InputIt src_end = &*src.cend();

    OutputIt dst_begin = reinterpret_cast<OutputIt>(&*dst.begin()) + dst_offset;
    OutputIt dst_it = dst_begin;
    OutputIt dst_end = reinterpret_cast<OutputIt>(&*dst.end());

    return ::llvm::ConvertUTF8toUTF32(&src_it, src_end, &dst_it, dst_end,
                                      ::llvm::ConversionFlags::strictConversion) ==
        ::llvm::ConversionResult::conversionOK ?
        dst_it - dst_begin :
        static_cast<ptrdiff_t>(DECODE_STATUS::ERROR);
}

#else /* U8D_USE_STD_STRING_VIEW */

ptrdiff_t
decode_next(const u8_char_t* src, size_t count, u32_char_t& code_point) noexcept {
    using InputIt = const ::llvm::UTF8*;
    using OutputIt = ::llvm::UTF32*;

    InputIt src_begin = src;
    InputIt src_it = src_begin;
    InputIt src_end = src_begin + count;

    OutputIt dst_begin = reinterpret_cast<OutputIt>(&code_point);
    OutputIt dst_end = dst_begin + 1;

    return ::llvm::ConvertUTF8toUTF32(&src_it, src_end, &dst_begin, dst_end,
                                      ::llvm::ConversionFlags::strictConversion) ==
        ::llvm::ConversionResult::conversionOK ?
        src_it - src_begin :
        static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::ERROR);
}

ptrdiff_t
decode(const u8_char_t* src, size_t count, u32_s_t& dst, size_t dst_offset) noexcept {
    using InputIt = const ::llvm::UTF8*;
    using OutputIt = ::llvm::UTF32*;

    InputIt src_begin = src;
    InputIt src_it = src_begin;
    InputIt src_end = src_begin + count;

    OutputIt dst_begin = reinterpret_cast<OutputIt>(&*dst.begin()) + dst_offset;
    OutputIt dst_it = dst_begin;
    OutputIt dst_end = reinterpret_cast<OutputIt>(&*dst.end());

    return ::llvm::ConvertUTF8toUTF32(&src_it, src_end, &dst_it, dst_end,
                                      ::llvm::ConversionFlags::strictConversion) ==
        ::llvm::ConversionResult::conversionOK ?
        dst_it - dst_begin :
        static_cast<ptrdiff_t>(DECODE_STATUS::ERROR);
}

#endif /* U8D_USE_STD_STRING_VIEW */

} // namespace llvm
} // namespace utf8decoders

// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#ifndef LIBUTF8DECODERS_KEWB_UTF8DECODERS_KEWB_HPP
#define LIBUTF8DECODERS_KEWB_UTF8DECODERS_KEWB_HPP

#include "libutf8decoders/config.hpp"
#include "libutf8decoders/features.hpp"

#include <cstddef>

#include "utf_utils.h"

#include "libutf8decoders/api.hpp"
#include "libutf8decoders/types.hpp"

namespace utf8decoders {
namespace kewb {

using ::utf8decoders::size_t;
using ::utf8decoders::ptrdiff_t;
using ::utf8decoders::u32_char_t;
using ::utf8decoders::u32_s_t;
#ifdef U8D_USE_STD_STRING_VIEW
using ::utf8decoders::u8_sv_t;
#else
using ::utf8decoders::u8_char_t;
#endif

using ::utf8decoders::DECODE_NEXT_STATUS;

using decode_next_f = ::std::int32_t (*)(const u8_char_t*& pSrc,
                                         const u8_char_t* pSrcEnd,
                                         char32_t& cdpt) U8D_NOEXCEPT_TYPE_SPEC;

using decode_f = ptrdiff_t (*)(const u8_char_t* pSrc,
                               const u8_char_t* pSrcEnd,
                               char32_t* pDst) U8D_NOEXCEPT_TYPE_SPEC;

#ifdef U8D_USE_STD_STRING_VIEW

template<decode_next_f dn>
U8D_CONSTEXPR ptrdiff_t
decode_next(u8_sv_t src, u32_char_t& code_point) noexcept {
    using InputIt = const u8_char_t*;

    InputIt src_begin = &*src.cbegin();
    InputIt src_it = src_begin;
    InputIt src_end = &*src.cend();

    return src_begin == src_end || dn(src_it, src_end, code_point) != ::uu::UtfUtils::State::ERR ?
        src_it - src_begin :
        static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::ERROR);
}

template<decode_f d>
ptrdiff_t
decode(u8_sv_t src, u32_s_t& dst, size_t dst_offset = 0) noexcept {
    using InputIt = const u8_char_t*;
    using OutputIt = u32_s_t::pointer;

    InputIt src_begin = &*src.cbegin();
    InputIt src_end = &*src.cend();
    OutputIt dst_begin = &*dst.begin() + dst_offset;

    return d(src_begin, src_end, dst_begin);
}

#else /* U8D_USE_STD_STRING_VIEW */

template<decode_next_f dn>
U8D_CONSTEXPR ptrdiff_t
decode_next(const u8_char_t* src, size_t count, u32_char_t& code_point) noexcept {
    using InputIt = const u8_char_t*;

    InputIt src_begin = src;
    InputIt src_it = src_begin;
    InputIt src_end = src_begin + count;

    return src_begin == src_end || dn(src_it, src_end, code_point) != ::uu::UtfUtils::State::ERR ?
        src_it - src_begin :
        static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::ERROR);
}

template<decode_f d>
ptrdiff_t
decode(const u8_char_t* src, size_t count, u32_s_t& dst, size_t dst_offset = 0) noexcept {
    using InputIt = const u8_char_t*;
    using OutputIt = u32_s_t::pointer;

    InputIt src_begin = src;
    InputIt src_end = src_begin + count;
    OutputIt dst_begin = &*dst.begin() + dst_offset;

    return d(src_begin, src_end, dst_begin);
}

#endif /* U8D_USE_STD_STRING_VIEW */

} // namespace kewb
} // namespace utf8decoders

#endif /* LIBUTF8DECODERS_KEWB_UTF8DECODERS_KEWB_HPP */

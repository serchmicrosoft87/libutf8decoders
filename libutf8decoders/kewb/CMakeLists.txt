add_library(utf8decoders-kewb
  utf8decoders-kewb.hpp
)

find_package(kewb-utf8 CONFIG REQUIRED)

# PUBLIC because kewb-utf8 is an OBJECT library so we must link to this library
# (to include them in this library) as well as its ancestors (which will not
# link the object files if treated as an interface)
target_link_libraries(utf8decoders-kewb PUBLIC kewb::kewb-utf8)

set_target_properties(utf8decoders-kewb PROPERTIES
  VERSION "${API_SO_MAJOR}.${API_SO_MINOR}.${API_SO_PATCH}"
  SOVERSION ${API_SO_MAJOR}
  INTERPROCEDURAL_OPTIMIZATION TRUE
)

target_compile_options(utf8decoders-kewb PRIVATE
  ${U8D_COMPILER_WARNINGS}
)

set(BUILD_INTERFACE_INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/../..;${CMAKE_CURRENT_BINARY_DIR}/../..)

target_include_directories(utf8decoders-kewb PUBLIC
  "$<BUILD_INTERFACE:${BUILD_INTERFACE_INCLUDE_DIRS}>"
  $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>
)

install(TARGETS utf8decoders-kewb EXPORT utf8decoders-kewb-targets
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
  PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/libutf8decoders/kewb
)

install(FILES
  ${CMAKE_CURRENT_SOURCE_DIR}/utf8decoders-kewb.hpp
  DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/libutf8decoders/kewb
)

set(ConfigPackageLocation ${CMAKE_INSTALL_LIBDIR}/cmake/utf8decoders)

install(EXPORT utf8decoders-kewb-targets
  FILE utf8decoders-kewb-targets.cmake
  NAMESPACE utf8decoders::
  DESTINATION ${ConfigPackageLocation}
)

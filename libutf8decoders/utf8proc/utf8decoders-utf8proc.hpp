// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#ifndef LIBUTF8DECODERS_UTF8PROC_UTF8DECODERS_UTF8PROC_HPP
#define LIBUTF8DECODERS_UTF8PROC_UTF8DECODERS_UTF8PROC_HPP

#include "libutf8decoders/config.hpp"
#include "libutf8decoders/features.hpp"

#include <cstdint>

#include "utf8proc.h"

#include "libutf8decoders/types.hpp"

namespace utf8decoders {
namespace utf8proc {

using ::utf8decoders::size_t;
using ::utf8decoders::ptrdiff_t;
using ::utf8decoders::u32_char_t;
using ::utf8decoders::u32_s_t;
#ifdef U8D_USE_STD_STRING_VIEW
using ::utf8decoders::u8_sv_t;
#else
using ::utf8decoders::u8_char_t;
#endif

#ifdef U8D_USE_STD_STRING_VIEW

ptrdiff_t
decode_next(u8_sv_t src, u32_char_t& code_point) noexcept;

#else /* U8D_USE_STD_STRING_VIEW */

ptrdiff_t
decode_next(const u8_char_t* src, size_t count, u32_char_t& code_point) noexcept;

#endif /* U8D_USE_STD_STRING_VIEW */

} // namespace utf8proc
} // namespace utf8decoders

#endif /* LIBUTF8DECODERS_UTF8PROC_UTF8DECODERS_UTF8PROC_HPP */

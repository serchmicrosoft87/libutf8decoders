// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#ifndef LIBUTF8DECODERS_BRANCHLESS_UTF8DECODERS_BRANCHLESS_HPP
#define LIBUTF8DECODERS_BRANCHLESS_UTF8DECODERS_BRANCHLESS_HPP

#include "libutf8decoders/config.hpp"
#include "libutf8decoders/features.hpp"

#include "branchless-utf8/utf8.h"

#include "libutf8decoders/api.hpp"
#include "libutf8decoders/types.hpp"

namespace utf8decoders {
namespace branchless {

using ::utf8decoders::size_t;
using ::utf8decoders::ptrdiff_t;
using ::utf8decoders::u32_char_t;
using ::utf8decoders::u32_s_t;
#ifdef U8D_USE_STD_STRING_VIEW
using ::utf8decoders::u8_sv_t;
#else
using ::utf8decoders::u8_char_t;
#endif

using ::utf8decoders::DECODE_NEXT_STATUS;

#ifdef U8D_USE_STD_STRING_VIEW

U8D_CONSTEXPR ptrdiff_t
decode_next(u8_sv_t src, u32_char_t& code_point) noexcept {
    using InputIt = unsigned char*;
    using size_t = u8_sv_t::size_type;

    size_t src_size = src.size();

    if (src_size == 0)
        return static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::OK);

    InputIt src_begin = const_cast<InputIt>(&*src.begin());

    int e{};

    InputIt src_it = ::utf8_decode(src_begin, &code_point, &e);

    return e == 0 ? src_it - src_begin : static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::ERROR);
}

ptrdiff_t
decode(u8_sv_t src, u32_s_t& dst, size_t dst_offset = 0) noexcept;

#else /* U8D_USE_STD_STRING_VIEW */

U8D_CONSTEXPR ptrdiff_t
decode_next(const u8_char_t* src, size_t count, u32_char_t& code_point) noexcept {
    using InputIt = unsigned char*;

    size_t src_size = count;

    if (src_size == 0)
        return static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::OK);

    InputIt src_begin = const_cast<InputIt>(src);

    int e{};

    InputIt src_it = ::utf8_decode(src_begin, &code_point, &e);

    return e == 0 ? src_it - src_begin : static_cast<ptrdiff_t>(DECODE_NEXT_STATUS::ERROR);
}

ptrdiff_t
decode(const u8_char_t* src, size_t count, u32_s_t& dst, size_t dst_offset = 0) noexcept;

#endif /* U8D_USE_STD_STRING_VIEW */

} // namespace branchless
} // namespace utf8decoders

#endif /* LIBUTF8DECODERS_BRANCHLESS_UTF8DECODERS_BRANCHLESS_HPP */

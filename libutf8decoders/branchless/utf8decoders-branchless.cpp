// SPDX-License-Identifier: MIT
// Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

#include "libutf8decoders/branchless/utf8decoders-branchless.hpp"

#include <cstdint>

namespace utf8decoders {
namespace branchless {

using ::utf8decoders::DECODE_STATUS;

#ifdef U8D_USE_STD_STRING_VIEW

ptrdiff_t
decode(u8_sv_t src, u32_s_t& dst, size_t dst_offset) noexcept {
    using InputIt = unsigned char*;
    using OutputIt = u32_s_t::pointer;

    InputIt src_it = const_cast<InputIt>(&*src.begin());
    InputIt src_end = const_cast<InputIt>(&*src.end());
    OutputIt dst_begin = &*dst.begin() + dst_offset;
    OutputIt dst_it = dst_begin;

    int e{};

    while (src_it < src_end && e == 0) {
        src_it = ::utf8_decode(src_it, dst_it++, &e);
    }
    return e == 0 ? dst_it - dst_begin : static_cast<ptrdiff_t>(DECODE_STATUS::ERROR);
}

#else /* U8D_USE_STD_STRING_VIEW */

ptrdiff_t
decode(const u8_char_t* src, size_t count, u32_s_t& dst, size_t dst_offset) noexcept {
    using InputIt = unsigned char*;
    using OutputIt = u32_s_t::pointer;

    InputIt src_it = const_cast<InputIt>(src);
    InputIt src_end = src_it + count;
    OutputIt dst_begin = &*dst.begin() + dst_offset;
    OutputIt dst_it = dst_begin;

    int e{};

    while (src_it < src_end && e == 0) {
        src_it = ::utf8_decode(src_it, dst_it++, &e);
    }
    return e == 0 ? dst_it - dst_begin : static_cast<ptrdiff_t>(DECODE_STATUS::ERROR);
}

#endif /* U8D_USE_STD_STRING_VIEW */

} // namespace branchless
} // namespace utf8decoders
